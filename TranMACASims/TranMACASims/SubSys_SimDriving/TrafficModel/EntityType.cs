namespace SubSys_SimDriving
{
	internal enum EntityType
	{
		RoadNetWork,
		 
		MobileEntity,
		 
		TrafficLight,
		 
		VMS,
		 
		Road,
		 
		RoadSegment,
		 
		RoadLane,
	 
	}
	 
}
 
